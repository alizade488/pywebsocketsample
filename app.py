from flask import Flask, render_template
import _thread as thread
import time
import socket
import os
from os.path import join, dirname
import threading
from dotenv import load_dotenv
from module import Vehicle, session, sessionmaker
from sqlalchemy import create_engine
import socket
import asyncio
import websockets
import pickle
from websocket import create_connection
import json
from multiprocessing import Process, Queue
from socket_client import WebSocketClient
from sqs import send, receive
import schedule


# pip install flask-socketio
# npm install -g wscat
# pip install websocket-client
# pip install Flask-SqlAlchemy
# pip install websockets
# pip install mysqlclient
# pip install rq
# pip freeze > requirements.txt
# from models.vehicles import db
# db.create_all()

DOTENV_PATH = join(dirname(__file__), '.env')
load_dotenv(dotenv_path=DOTENV_PATH)
APP = Flask(__name__)
APP.config['SQLALCHEMY_DATABASE_URI'] = os.getenv("DATABASE_URL")
APP.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
socket_api_key = os.getenv("SOCKET_API_KEY")


IS_RUNNIG = False
@APP.route('/start')
def startGetingData ():
    global IS_RUNNIG
    if not IS_RUNNIG:
        # qu = threading.Thread(target=open_socket())
        # qu.setDaemon(True)
        # qu.start()
        p = Process(target=open_socket)
        p.start()
        IS_RUNNIG = True
        return 'starting...'
    return "it's already running"


@APP.route('/')
def root():
    return "Recoy EV microservice"


@APP.route('/status')
def status():
    if not IS_RUNNIG:
        return 'Not started yet'
    return "It's already running"


def store_vehicles(vehicles):
    s = session()
    for vehicle in vehicles:
        vaheicle_data = vehicles[vehicle]
        print(vaheicle_data)
        v = Vehicle(vehicle_id = vehicle, soc = vehicles[vehicle][0] , gps = vehicles[vehicle][1])
        s.add(v)
        s.commit()
    s.close()
    return ''



def open_socket(test = False):
    client = WebSocketClient()
    loop = asyncio.new_event_loop()
    asyncio.set_event_loop(loop)
    connection = asyncio.get_event_loop().run_until_complete(client.async_processing(test= test))
    return 'done!'
    

def check_sqs():
    second = int(os.getenv("check_queue_repeat_time"))
    sqs_queue()
    schedule.every(second).seconds.do(sqs_queue)
    while True:
        schedule.run_pending()
        time.sleep(1)



def sqs_queue():
    operations_queue_url = str(os.getenv("operations_queue_url"))
    receive_msg = receive(operations_queue_url)
    if receive_msg != []:
        print(receive_msg)
        if 'type' in receive_msg['MessageAttributes']:
            if receive_msg['MessageAttributes']['type']['StringValue'] == 'EvDataRequest':
                session_update_model = json.loads(receive_msg['Body'])
                print(session_update_model)
                last_vehicle = session.query(Vehicle).order_by(Vehicle.id.desc()).filter(
                    Vehicle.gps != 'test').first()
                
                return ""
    return ""


def sqs_response (last_vehicle):
    ocpi_queuq_url = str(os.getenv("ocpi_queue_url"))
    if last_vehicle == None:
        send(
            {'type': {'DataType': 'String', 'StringValue': 'EvDataMessage'},},
            'None', ocpi_queuq_url)
    else:
        send(
            {'type': {'DataType': 'String', 'StringValue': 'EvDataMessage'},},
            json.dumps(last_vehicle), ocpi_queuq_url)

if __name__ == '__main__':
    APP.run(host='0.0.0.0', port='5002', debug=True)
    
