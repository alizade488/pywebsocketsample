import asyncio
import websocket
import ssl
import app
from dotenv import load_dotenv
from websocket import create_connection
import os
from os.path import join, dirname
import json

DOTENV_PATH = join(dirname(__file__), '.env')
load_dotenv(dotenv_path=DOTENV_PATH)
socket_api_key = os.getenv("SOCKET_API_KEY")

class WebSocketClient():
    
    def __init__(self):
        pass
    
    async def async_processing(self, test = False):
        uri = f"wss://sdk.viriciti.com/api/v2/live?apiKey={socket_api_key}"
        # uri = "ws://localhost:8765"
        ws = create_connection(uri)
        # ws = websocket.WebSocket(sslopt={'cert_reqs':ssl.CERT_NONE})
        while True:
            try:
                ws.connect(uri)
                if test == True:
                    # test_resp = ws.recv()-> when socket server start geting data we can turn on it
                    print("It was just a test!")
                    break    
                res = json.loads(ws.recv())
                app.store_vehicles(res['vehicles'])
            except Exception as e:
                print(f'ConnectionClosed cause {e}!')
                break