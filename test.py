from datetime import datetime
import random
import os
from os.path import join, dirname
from time import sleep
import pytest
from dotenv import load_dotenv
from sqlalchemy.orm import sessionmaker
from app import APP as _app , store_vehicles
from module import ENGINE, Vehicle , Base
from app import socket_api_key, open_socket
from threading import Thread
import time
from multiprocessing import Process, Queue
from module import Vehicle, session, sessionmaker
from sqs import SQS, receive, send

@pytest.fixture
def client(app):
    """
    flask client
    """
    return app.test_client()

def test_start_page(client):
    """
    test_start_page
    """
    response = client.get('/start')
    assert response.status_code == 200

def test_status_page(client):
    """
    test_status_page
    """
    response = client.get('/status')
    assert response.status_code == 200

def test_open_secket():
    assert open_socket(test=True)

def test_store_vehicles():
    d = {
        "vehicles": {
            "vehicle_1":["55.5555","test"],
            "vehicle_2":["55.5555","test"],
        }
    }
    
    store_vehicles(d['vehicles'])


def test_aws_sqs_connect():
    SQS.list_queues()


def test_sqs_send_msg():
    data_collection_queue_url = str(os.getenv("data_collection_queue_url"))
    response = send({
        'test': {
            'DataType': 'String',
            'StringValue': 'test'
        }}, 'test', data_collection_queue_url)
    assert response != ''

def test_sqs_receive_msg():
    data_collection_queue_url = str(os.getenv("data_collection_queue_url"))
    receive(data_collection_queue_url)



@pytest.fixture
def app():
    """
    run test
    """
    with _app.app_context():
        yield _app
